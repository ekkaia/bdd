----- Requetes simples

-- Compter le nombre d'artiste dans Spotify
SELECT count(distinct artist_id)
FROM artist

-- Compter le nombre d'artiste dans Le Billboard par rapport a Spotify
SELECT artist_name, count(album_id) as total_album
FROM artist natural join track_fact natural join album
GROUP BY artist_name
ORDER BY total_album desc

-- Moyenne d'album par artiste
SELECT AVG(total_album)
FROM (SELECT artist_name, count(album_id) as total_album
	FROM artist natural join track_fact natural join album
	GROUP BY artist_name
	ORDER BY total_album desc ) as grp




----- Requetes OLAP

-- Afficher le rang des artistes entre le Billboard et Spotify
SELECT count(distinct artist_id)
FROM rank_fact natural join track_fact natural join artist

-- CUBE sur le nombre de musique par album pour chaque artiste
SELECT artist_name, album_name, count(track_id) as total_track
FROM artist natural join album natural join track_fact natural join track
GROUP BY cube(artist_name, album_name)
ORDER BY artist_name
limit 15

-- ROLLUP sur les genres et sous genres des musiques présentent entre Spotify et le Billboard
SELECT genre_name,sub_genre_name, count(genre_name) as total_genre
FROM sub_genre natural join genre natural join track_fact natural join rank_fact
GROUP BY ROLLUP(sub_genre_name, genre_name)
ORDER BY sub_genre_name
limit 15

-- Affiche le rang de chaque artiste classés dans le BillBoard par rapport a leur nombre de semaine
SELECT artist_name, count(distinct week) as total_week,
rank() over (ORDER BY count(distinct week) desc) rank
FROM artist natural join track_fact natural join rank_fact
GROUP BY artist_name
ORDER BY total_week desc
limit 20

-- Calcul le cumul des semaines des artistes classés dans le billboard
SELECT artist_name, count(distinct week) as total_week,
SUM(count(distinct week))
OVER(ORDER BY count(distinct week) desc
ROWS unbounded preceding)
FROM artist natural join track_fact natural join rank_fact
GROUP BY artist_name
ORDER BY total_week desc
limit 100


-- Calcul la proportion de semaine de classement pour chaque artiste, ex, Drake est présent 42% du temps dans le Billboard 100, sans tenir compte de la position
SELECT artist_name, total_artist, (total_artist/total) * 100.0 as percentage, total
FROM (
      SELECT count(distinct week) as total_artist, artist_name, (
                SELECT  1.0 * count(distinct week)
                FROM rank_fact ) as total
     FROM artist natural join track_fact natural join rank_fact
     GROUP BY artist_name
     ORDER BY count(distinct week)desc
) as test

Limit 50

-- Calcul la part des genres présents dans le billboard
SELECT genre_name as genre, total_genre_billboard, (total_genre_billboard/total) * 100 as proportion_billboard, NTILE (3) OVER (
ORDER BY total_genre_billboard desc
) NTILE
FROM (
      SELECT count(week_position) as total_genre_billboard, genre_name, (
                SELECT  1.0 * count(week)
                FROM rank_fact ) as total
     FROM genre natural join track_fact natural join rank_fact
     GROUP BY genre_id
     ORDER BY count(week_position)desc
) as test
Limit 50

-- Calcule la moyenne des tempos des musiques de chaque semaine et affiche le décalage entre celui de la semaine precedente et l'actuelle
SELECT week, AVG_tempo,  (AVG_tempo - LAG(AVG_tempo, 1,0) OVER(ORDER BY week)) as te
FROM (SELECT week, SUM(tempo) / count(week) as AVG_tempo
    FROM track_fact natural join rank_fact
    GROUP BY week
    ORDER BY week) as test



