const knex = require("knex")(require("./knexfile"));
const prompt = require("prompt");

async function startRequest(reqNum) {
	try {
		let result;
		switch (reqNum) {
			case 1:
				result = await knex.schema.raw(`
					SELECT count(distinct artist_id)
					FROM artist
				`);
				break;
			case 2:
				result = await knex.schema.raw(`
					SELECT artist_name, count(album_id) as total_album
					FROM artist natural join track_fact natural join album
					GROUP BY artist_name
					ORDER BY total_album desc
				`);
				break;
			case 3:
				result = await knex.schema.raw(`
					SELECT AVG(total_album)
					FROM (SELECT artist_name, count(album_id) as total_album
					FROM artist natural join track_fact natural join album
					GROUP BY artist_name
					ORDER BY total_album desc ) as grp
				`);
				break;
			case 4:
				result = await knex.schema.raw(`
					SELECT count(distinct artist_id)
					FROM rank_fact natural join track_fact natural join artist
				`);
				break;
			case 5:
				result = await knex.schema.raw(`
					SELECT artist_name, album_name, count(track_id) as total_track
					FROM artist natural join album natural join track_fact natural join track
					GROUP BY cube(artist_name, album_name)
					ORDER BY artist_name
					limit 15
				`);
				break;
			case 6:
				result = await knex.schema.raw(`
					SELECT genre_name,sub_genre_name, count(genre_name) as total_genre
					FROM sub_genre natural join genre natural join track_fact natural join rank_fact
					GROUP BY ROLLUP(sub_genre_name, genre_name)
					ORDER BY sub_genre_name
					limit 15
				`);
				break;
			case 6:
				result = await knex.schema.raw(`
					SELECT artist_name, count(distinct week) as total_week,
					rank() over (ORDER BY count(distinct week) desc) rank
					FROM artist natural join track_fact natural join rank_fact
					GROUP BY artist_name
					ORDER BY total_week desc
					limit 20
				`);
				break;
			case 8:
				result = await knex.schema.raw(`
					SELECT artist_name, count(distinct week) as total_week,
					SUM(count(distinct week))
					OVER(ORDER BY count(distinct week) desc
					ROWS unbounded preceding)
					FROM artist natural join track_fact natural join rank_fact
					GROUP BY artist_name
					ORDER BY total_week desc
					limit 100
				`);
				break;
			case 9:
				result = await knex.schema.raw(`
				SELECT artist_name, total_artist, (total_artist/total) * 100.0 as percentage, total
				FROM (
					SELECT count(distinct week) as total_artist, artist_name, (
							SELECT  1.0 * count(distinct week)
							FROM rank_fact ) as total
					FROM artist natural join track_fact natural join rank_fact
					GROUP BY artist_name
					ORDER BY count(distinct week)desc
				) as test
				Limit 50
				`);
				break;
			case 10:
				result = await knex.schema.raw(`
				SELECT genre_name as genre, total_genre_billboard, (total_genre_billboard/total) * 100 as proportion_billboard, NTILE (3) OVER (
					ORDER BY total_genre_billboard desc
					) NTILE
					FROM (
						SELECT count(week_position) as total_genre_billboard, genre_name, (
									SELECT  1.0 * count(week)
									FROM rank_fact ) as total
						FROM genre natural join track_fact natural join rank_fact
						GROUP BY genre_id
						ORDER BY count(week_position)desc
					) as test
					Limit 50
				`);
				break;
			case 11:
				result = await knex.schema.raw(`
					SELECT week, AVG_tempo,  (AVG_tempo - LAG(AVG_tempo, 1,0) OVER(ORDER BY week)) as te
					FROM (SELECT week, SUM(tempo) / count(week) as AVG_tempo
						FROM track_fact natural join rank_fact
						GROUP BY week
						ORDER BY week) as test
				`);
				break;
			default:
				throw new Error("Entrez un nombre compris entre 1 et 11")
		}
		console.log(result);
	} catch (error) {
		console.error(error);
		process.exit(1);
	}
}

(async () => {
	try {
		console.log("Entrez un nombre compris entre 1 et 11");
		prompt.start();
		prompt.get(["input"], (err, result) => {
			startRequest(parseInt(result.input));
		});
	} catch (error) {
		console.error(error);
		process.exit(1);
	}
})();
