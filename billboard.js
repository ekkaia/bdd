const knex = require("knex")(require("./knexfile"));
const csv = require("csv-parser");
const fs = require("fs");
const dotenv = require("dotenv");

// Variables d'environnement
dotenv.config();

// Pourcentage de progression dans l'import
let percent = null;
// Pourcentage de musiques sautées car pas de correspondance avec les données de Spotify
let skipped = 0;
// Pourcentages de musiques ayant plusieurs correspondances avec les données de Spotify (ambiguïté)
let issue = 0;

/**
 * Charge un CSV et retourne son équivalent en JSON
 * @param {string} csvName Nom du fichier CSV
 * @returns Promesse JSON du CSV parsé
 */
async function dataLoader(csvName) {
	console.log(`Chargement des données ${csvName}...`);
	return new Promise((resolve, reject) => {
		let csvdata = [];
		fs.createReadStream(__dirname + `/data/${csvName}.csv`)
			.pipe(csv({ separator: "," }))
			.on("data", (d) => csvdata.push(d))
			.on("end", () => {
				resolve(csvdata);
			})
			.on("error", (err) => reject(err));
	});
}

/**
 * Retourne la sous-chaîne inclue entre le début de la chaîne et la première apparition du pattern
 * @param {string} str Chaîne à traiter
 * @param {string} pattern Motif à matcher
 * @returns Sous-chaîne
 */
function truncateAfter(str, pattern) {
	return str.indexOf(pattern) === -1 ? str : str.slice(0, str.indexOf(pattern));
} 

/**
 * Script d'import Billboard
 * @param {Object} data JSON du CSV parsé
 */
async function importBillboard(data) {
	console.log("Démarrage du script d'import du csv billboard...");
	// Parcours des données
	for (const [i, el] of data.entries()) {
		// Progression de l'import du CSV
        const progress = Math.floor(100 * i/data.length);
		if (percent !== progress) { 
			console.info(`${progress}% complete... ${Math.floor(100 * skipped/data.length)}% skipped... ${Math.floor(100 * issue/data.length)}% songs with possible issue...`);
			percent = progress;
		}

		// Nom de la musique, sans la partie "featuring"
		const truncatePerformer = truncateAfter(el.performer, " Featuring").replace(/'/g, "\\'");
		const truncateSong = el.song.replace(/'/g, "\\'");

		// Id de la musique correspondant
		const track_id = await knex.schema.raw(`
			SELECT "track_id"
			FROM "track_fact" NATURAL JOIN "track" NATURAL JOIN "artist" NATURAL JOIN "album"
			WHERE unaccent("track".track_name) ILIKE unaccent(E'%${truncateSong}%')
			AND unaccent("artist".artist_name) ILIKE unaccent(E'%${truncatePerformer}%')
		`);

		// Si une musique a été trouvée
		if (track_id.rows[0] && track_id.rows[0].track_id) {
            // Si plus d'une musique correspondent
			if (track_id.rows.length > 1) {
                issue += 1;
            }
			// Insertion dans la table des faits
			await knex("rank_fact").insert({
				track_id: track_id.rows[0].track_id,
				week_position: el.week_position,
				previous_week_position: el.previous_week_position === "NA" ? null : el.previous_week_position,
				week: el.week_id,
				peak_position: el.peak_position,
				weeks_on_chart: el.weeks_on_chart,
			});
		} else {
			skipped += 1;
		}
	}
}

// Fonction qui se lance à l'import
(async () => {
	try {
		// Calcul du temps d'import
		console.time("Import time:");

		const billboard = await dataLoader("billboard");
		await importBillboard(billboard);

		console.timeEnd("Import time:");
		console.log("Import status : success 🥳");
		process.exit(0);
	} catch (error) {
		console.log("Import status : failed 🤬");
		console.error(error);
		process.exit(1);
	}
})();
