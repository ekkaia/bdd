const path = require("path");
const dotenv = require("dotenv");

dotenv.config(); 

module.exports = {
	client: "pg",
	connection: process.env.PG_CONNECTION_STRING,
	pool: {
		min: parseInt(process.env.KNEX_POOL_MIN, 10),
		max: parseInt(process.env.KNEX_POOL_MAX, 10),
	},
	searchPath: ["public", "spotify"],
	migrations: {
		tableName: "knex_migrations",
	},
};
