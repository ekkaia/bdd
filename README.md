# Sujet

Entrepôt de données OLAP en PostgreSQL traitant de la relation entre les propriétés d'une musique et son classement dans le Billboard.

# Sources des données

Les deux datasets sont sous licence `Creative Commons Zero v1.0 Universal`.

* [CSV Billboard](https://github.com/rfordatascience/tidytuesday/tree/master/data/2021/2021-09-14)
* [CSV Spotify](https://github.com/rfordatascience/tidytuesday/tree/master/data/2020/2020-01-21)

Nos scripts d'import sont sous licence `GNU Affero 3`.

# Contributeurs

* Lou-Anne Sauvêtre
* Félix Saget
* Léo Olivier
* Sofiane Couëdel

# Installation

Ajoutez un fichier `.env` à la racine du projet. Il doit contenir les informations suivantes :

```
PG_CONNECTION_STRING=postgresql://root:root@localhost/spotify
KNEX_POOL_MAX=10
KNEX_POOL_MIN=1
```

```
docker compose up
npm i
knex migrate:latest
npm run spotify
npm run billboard
```

Testez les requêtes avec

```
npm run request-cli
```

# Utilisation

La BDD est lancée sur le port `8080`.
L'explorateur de BDD Adminer est lancé sur le port `8181`. Il est accessible depuis votre navigateur.

# Technologies utilisées & méthode

## Méthodes d'intégration/nettoyage des données

Nous nous sommes intéressés aux données du Billboard pour la décennie 2010. Cela nous a demandé de préalablement nettoyer les données en ne gardant du CSV que le classement 2010-2020.

Cette opération a été réalisée à l'aide d'un script Python disponible dans `/cleaning_billboard/csv_cleaner.py`.

Ensuite, nous avons utilisé le _SQL query builder_ [Knex](https://knexjs.org/) pour écrire des scripts d'import et gérer les _migrations_ des tables SQL.

Deux scripts Node JS réalisent l'import. On utilise `csv-parser` pour transformer les fichiers CSV en JSON. On itère ensuite dans l'objet et on importe les données dans la base avec Knex.

## Schéma

![Schéma en étoile de notre entrepôt de données](/star.png)

## Droits d'accès
