/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) => Promise.all([
    knex.schema.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'),
    knex.schema.raw('CREATE EXTENSION IF NOT EXISTS "pgcrypto";'),
    knex.schema.raw('CREATE EXTENSION IF NOT EXISTS "unaccent";'),
    knex.schema.raw('CREATE SCHEMA IF NOT EXISTS spotify;'),
]);

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => Promise.all([
    knex.schema.raw(`DROP SCHEMA IF EXISTS "spotify";`)
]);

