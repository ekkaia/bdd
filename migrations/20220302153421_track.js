/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) => Promise.all([
    knex.schema.raw(`
        CREATE TABLE "spotify".track (
            track_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
            track_name TEXT
        );
    `),
]);

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => Promise.all([
    knex.schema.raw(`DROP TABLE IF EXISTS "track";`)
]);
