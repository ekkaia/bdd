/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) => Promise.all([
    knex.schema.raw(`
        CREATE TABLE "spotify".curators (
            curator_name TEXT PRIMARY KEY, 
            curator_genre TEXT
        );
    `),

    knex.schema.raw(`ALTER TABLE genre ENABLE ROW LEVEL SECURITY;`),
    knex.schema.raw(`CREATE POLICY genre_curator ON genre TO spot_curator FOR SELECT
        USING (genre_name IN
            (SELECT curator_genre
            FROM "curators"
            WHERE curator_name = current_user) 
        );
    `),
]);

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => Promise.all([
    knex.schema.raw(`DROP TABLE IF EXISTS "curators";`),
    knex.schema.raw(`ALTER TABLE genre DISABLE ROW LEVEL SECURITY;`)
]);
