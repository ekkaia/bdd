/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) => Promise.all([
    knex.schema.raw(`
        CREATE TABLE "spotify".rank_fact (
            track_id UUID REFERENCES "spotify".track (track_id),
            week_position INTEGER CHECK (week_position >= 1 AND week_position <= 100),
            previous_week_position INTEGER CHECK (previous_week_position >= 1 AND previous_week_position <= 100),
            week TIMESTAMPTZ,
            peak_position INTEGER CHECK (peak_position >= 1 AND peak_position <= 100),
            weeks_on_chart INTEGER
        );
    `),
]);

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => Promise.all([
    knex.schema.raw(`DROP TABLE IF EXISTS "rank_fact";`)
]);
