/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) => Promise.all([
    knex.schema.raw(`
        CREATE TABLE "spotify".sub_genre (
            sub_genre_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
            sub_genre_name TEXT UNIQUE
        );
    `),
]);

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => Promise.all([
    knex.schema.raw(`DROP TABLE IF EXISTS "sub_genre";`)
]);
