/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) => Promise.all([
    knex.schema.raw(`
        CREATE TABLE "spotify".track_fact (
            track_id UUID REFERENCES "spotify".track (track_id),
            artist_id UUID REFERENCES "spotify".artist (artist_id),
            release_date_id UUID REFERENCES "spotify".release_date (release_date_id),
            album_id UUID REFERENCES "spotify".album (album_id),
            genre_id UUID REFERENCES "spotify".genre (genre_id),
            mode BOOLEAN,
            danceability REAL CHECK (danceability >= 0 AND danceability <= 1),
            energy REAL CHECK (energy >= 0 AND energy <= 1),
            loudness REAL CHECK (loudness >= -60 AND loudness <= 5),
            speechiness REAL CHECK (speechiness >= 0 AND speechiness <= 1),
            acousticness REAL CHECK (acousticness >= 0 AND acousticness <= 1),
            instrumentaless REAL CHECK (instrumentaless >= 0 AND instrumentaless <= 1),
            liveness REAL CHECK (liveness >= 0 AND liveness <= 1),
            valence REAL CHECK (valence >= 0 AND valence <= 1),
            tempo REAL CHECK (tempo >= 0 AND tempo <= 250),
            key INTEGER CHECK (key >= 0 AND key <= 11),
            duration INTERVAL 
        );
    `),
]);

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => Promise.all([
    knex.schema.raw(`DROP TABLE IF EXISTS "track_fact";`)
]);
