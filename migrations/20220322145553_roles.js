/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) => Promise.all([
    knex.schema.raw(`CREATE ROLE IF NOT EXISTS spot_user;`),
    knex.schema.raw(`CREATE ROLE IF NOT EXISTS spot_curator;`),
    knex.schema.raw(`CREATE ROLE IF NOT EXISTS spot_commercial;`),

    knex.schema.raw(`
        GRANT SELECT(track_id, artist_id, release_date_id, album_id, duration)
        ON track_fact
        TO spot_user;
    `),

    knex.schema.raw(`
        GRANT SELECT(track_id, week_position)
        ON rank_fact
        TO spot_user;
    `),
    
    knex.schema.raw(`
        GRANT SELECT
        ON track
        TO spot_user;
    `),
    
    knex.schema.raw(`
        GRANT SELECT
        ON artist
        TO spot_user;
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON release_date
        TO spot_user;
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON album
        TO spot_user;
    `),


    knex.schema.raw(`
        GRANT SELECT(track_id, artist_id, release_date_id, album_id, genre_id)
        ON track_fact
        TO spot_curator;
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON rank_fact
        TO spot_curator;
    `),
    
    knex.schema.raw(`
        GRANT SELECT
        ON track
        TO spot_curator;
    `),
    
    knex.schema.raw(`
        GRANT SELECT
        ON artist
        TO spot_curator;
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON release_date
        TO spot_curator;
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON album
        TO spot_curator;
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON genre
        TO spot_curator;
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON sub_genre
        TO spot_curator;
    `),


    knex.schema.raw(`
        GRANT SELECT
        ON track_fact
        TO spot_commercial;
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON rank_fact
        TO spot_commercial;    
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON artist
        TO spot_commercial;    
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON track
        TO spot_commercial;    
    `),

    knex.schema.raw(`
        GRANT SELECT
        ON album
        TO spot_commercial;    
    `)
]);

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => Promise.all([
    knex.schema.raw(`DROP ROLE IF EXISTS spot_user`),
    knex.schema.raw(`DROP ROLE IF EXISTS spot_curator`),
    knex.schema.raw(`DROP ROLE IF EXISTS spot_commercial`)
]);
