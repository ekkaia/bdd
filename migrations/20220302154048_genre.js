/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) => Promise.all([
    knex.schema.raw(`
        CREATE TABLE "spotify".genre (
            genre_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
            genre_name TEXT UNIQUE,
            sub_genre_id UUID REFERENCES "spotify".sub_genre (sub_genre_id)
        );
    `),
]);

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => Promise.all([
    knex.schema.raw(`DROP TABLE IF EXISTS "genre";`)
]);
