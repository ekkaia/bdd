const knex = require("knex")(require("./knexfile"));
const csv = require("csv-parser");
const fs = require("fs");
const dotenv = require("dotenv");

// Variables d'environnement
dotenv.config();

// Pourcentage de progression dans l'import
let percent = null;

/**
 * Charge un CSV et retourne son équivalent en JSON
 * @param {string} csvName Nom du fichier CSV
 * @returns Promesse JSON du CSV parsé
 */
async function dataLoader(csvName) {
	console.log(`Chargement des données ${csvName}...`);
	return new Promise((resolve, reject) => {
		let csvdata = [];
		fs.createReadStream(__dirname + `/data/${csvName}.csv`)
			.pipe(csv({ separator: "," }))
			.on("data", (d) => csvdata.push(d))
			.on("end", () => {
				resolve(csvdata);
			})
			.on("error", (err) => reject(err));
	});
}

/**
 * Script d'import Spotify
 * @param {Object} data JSON du CSV parsé
 */
async function importSpotify(data) {
	console.log("Démarrage du script d'import du csv spotify...");
	// Parcours des données
	for (const [i, el] of data.entries()) {
		// Progression de l'import du CSV
		const progress = Math.floor(100 * i/data.length);

		if (percent !== progress) { 
			console.info(`${progress}% complete...`);
			percent = progress;
		}
		
		// Insertion dans les tracks, retournant l'id du track
		const track_id = await knex("track")
			.insert({
				track_name: el.track_name,
			})
			.returning("track_id")
			.then((res) => res[0].track_id);

		// Insertion dans les artistes, si l'artiste est déjà dans la base, on ne fait rien
		await knex("artist")
			.insert({
				artist_name: el.track_artist,
			})
			.onConflict("artist_name")
			.ignore();

		// Récupération de l'id de l'artiste
		const artist_id = await knex("artist")
			.where({
				artist_name: el.track_artist,
			})
			.select("artist_id")
			.then((res) => res[0].artist_id);

		// Insertion dans les albums, retournant l'id de l'album
		const album_id = await knex("album")
			.insert({
				album_name: el.track_album_name,
			})
			.returning("album_id")
			.then((res) => res[0].album_id);

		// Insertion dans les sous-genres, si le sous-genre est déjà dans la base, on ne fait rien 
		await knex("sub_genre")
			.insert({
				sub_genre_name: el.playlist_subgenre,
			})
			.onConflict("sub_genre_name")
			.ignore();

		// Récupération de l'id du sous-genre
		const sub_genre_id = await knex("sub_genre")
			.where({
				sub_genre_name: el.playlist_subgenre,
			})
			.select("sub_genre_id");

		// Insertion dans les genres, en cas de conflit on ne fait rien
		await knex("genre")
			.insert({
				genre_name: el.playlist_genre,
				sub_genre_id: sub_genre_id[0].sub_genre_id,
			})
			.onConflict("genre_name")
			.ignore();

		// Récupération de l'id du genre
		const genre_id = await knex("genre")
			.where({
				genre_name: el.playlist_genre,
			})
			.select("genre_id")
			.then((res) => res[0].genre_id);

		// Formatage de la date
		const release_date =
			el.track_album_release_date.length <= 4
				? `${el.track_album_release_date}-01-01 00:00:00.000+00`
				: el.track_album_release_date.length <= 7
				? `${el.track_album_release_date}-01 00:00:00.000+00`
				: `${el.track_album_release_date} 00:00:00.000+00`;

		// Insertion dans les dates, retournant l'id de la date
		const release_date_id = await knex("release_date")
			.insert({
				date: release_date,
			})
			.returning("release_date_id")
			.then((res) => res[0].release_date_id);

		// Insertion dans la table des faits des tracks
		await knex("track_fact").insert({
			track_id,
			artist_id,
			release_date_id,
			album_id,
			genre_id,
			mode: el.mode,
			danceability: el.danceability,
			energy: el.energy,
			loudness: el.loudness,
			speechiness: el.speechiness,
			acousticness: el.acousticness,
			instrumentaless: el.instrumentaless,
			liveness: el.liveness,
			valence: el.valence,
			tempo: el.tempo,
			key: el.key,
			duration: el.duration_ms,
		});
	}
}

// Fonction qui se lance à l'import
(async () => {
	try {
		// Calcul du temps d'import
		console.time("Import time:");

		const spotify = await dataLoader("spotify");
		await importSpotify(spotify);

		console.timeEnd("Import time:");
		console.log("Import status : success 🥳");
		process.exit(0);
	} catch (error) {
		console.log("Import status : failed 🤬");
		console.error(error);
		process.exit(1);
	}
})();
